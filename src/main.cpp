//=============================================================================
//  @Author:        Vanzo Luca Samuele <luca.vanzo@gmail.com>
//  @Version:       1.0.100
//  @Revision:      1
//=============================================================================
// INIZIO PROGETTO GRAPH
//=============================================================================

#include "graph.h"
using namespace std;

int main()
{
    Graph g;

    // NODI!!!!!!
    cout << endl;

    Node* tmp = g.appendNode();
    tmp->name = "pippo";

    tmp = g.appendNode();
    tmp->name = "pluto";

    tmp = g.appendNode();
    tmp->name = "paperino";

    g.outputGraph();

    // ARCHI!!!!!!
    cout << "TEST:  Aggiunta archi!" << endl;

    if(!g.appendArc("pluto", "pippo"))
        cout << "CREATE-ARC:  Error!!" << endl;

    if(!g.appendArc("pippo", "pippo"))
        cout << "CREATE-ARC:  Error!!" << endl;

    if(!g.appendArc("pippo", "paperino"))
        cout << "CREATE-ARC:  Error!!" << endl;

    if(!g.appendArc("paperino", "pluto"))
        cout << "CREATE-ARC:  Error!!" << endl;

    if(!g.appendArc("paperino", "paperino"))
        cout << "CREATE-ARC:  Error!!" << endl;

    g.outputGraph();

    // RIMOZIONE ARCHI
    cout << "TEST:  Rimozione archi!" << endl;

    if(!g.removeArc("pippo", "pippo"))
        cout << "REMOVE-ARC:  Error!!" << endl;

    g.outputGraph();

    // ARCHI!!!!!!
    cout << "TEST:  Aggiunta nodi in posizione X!" << endl;

    tmp = g.insertNode(1);
    tmp->name = "topolino";

    tmp = g.insertNode(2);
    tmp->name = "tigro";

    g.outputGraph();

    // ARCHI!!!!!!
    cout << "TEST:  Rimozione nodi in posizione X!" << endl;

    if(!g.removeAt(3))
        cout << "REMOVE-NODE:  Error!!" << endl;

    g.outputGraph();

    // ARCHI!!!!!!
    cout << "TEST:  Resize grafo a ZERO (force = true)!" << endl;

    g.resizeGraph(0, true);
    g.outputGraph();

    return 1;
}

//=============================================================================
//                                   EOF
//=============================================================================
