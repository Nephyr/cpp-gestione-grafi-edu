//=============================================================================
//  @Author:        Vanzo Luca Samuele <luca.vanzo@gmail.com>
//  @Version:       1.0.100
//  @Revision:      1
//=============================================================================
// INIZIO PROGETTO GRAPH
//=============================================================================

#ifndef __GRAPH_H__
#define __GRAPH_H__
#include <iostream>

// Definisco la struttura NODO
typedef struct __node Node;

// Dichiaro la struttura NODO
typedef struct __node
{
    // Membri nodo
    std::string name;
    int nodeId;

    // Membri archi
    int countArcs;
    Node** arcs;
} Node;

//=============================================================================
//  CLASSE GRAFO
//=============================================================================

class Graph
{
private:
//=============================================================================
//  MEMBRI PRIVATI CLASSE
//=============================================================================

    int         __countNodes;
    Node*       __nodes;

//=============================================================================
//  METODI NODI PRIVATI CLASSE
//=============================================================================

    bool __resize(int newSize, int offset = -1, bool direct = false)
    {
        if(__countNodes < 0)
            return false;

        if(newSize <= 0)
        {
            for(int i = 0; i < __countNodes; i++)
                delete[] __nodes[i].arcs;

            delete[] __nodes;

            __nodes      = NULL;
            __countNodes = 0;

            return true;
        }

        if(offset >= 0 && direct)
        {
            for(int i = offset; i < __countNodes; i++)
            {
                if((i + 1) >= __countNodes)
                    continue;

                __nodes[i].nodeId       = i;
                __nodes[i].name         = __nodes[i + 1].name;
            }
        }

        Node* tmpPtr     = __nodes;
        int tmpCount     = __countNodes;
        int current      = 0;
        __countNodes     = newSize;
        __nodes          = new Node[__countNodes];

        if(!__nodes)
        {
            __countNodes = tmpCount;
            __nodes      = tmpPtr;
            tmpPtr       = NULL;

            return false;
        }

        if (tmpCount > newSize) current = newSize;
        else current = tmpCount;

        for(int i = 0; i < current; i++)
        {
            __nodes[i].nodeId           = i;
            __nodes[i].name             = tmpPtr[i].name;
            __nodes[i].arcs             = NULL;
            __nodes[i].countArcs        = 0;
        }

        if (tmpCount < newSize)
            for(int i = current; i < newSize; i++)
            {
                __nodes[i].nodeId       = i;
                __nodes[i].name         = "__Unknown__";
                __nodes[i].arcs         = NULL;
                __nodes[i].countArcs    = 0;
            }

        if(offset >= 0 && !direct)
        {
            for(int i = __countNodes - 1; i > offset; i--)
            {
                if((i - 1) < 0)
                    continue;

                __nodes[i].nodeId       = i;
                __nodes[i].name         = __nodes[i - 1].name;
            }

            __nodes[offset].name        = "__Unknown__";
        }

        if(!__connectionUpdater(tmpPtr, tmpCount))
        {
            __countNodes = tmpCount;
            __nodes      = tmpPtr;
            tmpPtr       = NULL;

            return false;
        }
        else
        {
            delete[] tmpPtr;
            tmpPtr = NULL;

            return true;
        }
    }

    bool __findNodes(std::string src, std::string dst, int* srcFound, int* dstFound)
    {
        if(__countNodes <= 0)
            return false;

        int srcF = -1;
        int dstF = -1;

        for(int i = 0; i < __countNodes; i++)
        {
            if(__nodes[i].name.compare(src) == 0)
                srcF = __nodes[i].nodeId;

            if(__nodes[i].name.compare(dst) == 0)
                dstF = __nodes[i].nodeId;
        }

        if(srcF < 0 || dstF < 0)
            return false;
        else
        {
            (*srcFound) = srcF;
            (*dstFound) = dstF;
            return true;
        }
    }

    bool __connectionCleaner(int srcId)
    {
        int countSrc = __nodes[srcId].countArcs;

        if(countSrc <= 0)
            return true;

        for(int i = countSrc - 1; i >= 0; i--)
        {
            if(!__disconnectNodes(srcId, __nodes[srcId].arcs[i]->nodeId))
                return false;
        }

        return true;
    }

    bool __connectionUpdater(Node* oldGraph, int sizeGraph)
    {
        for(int i = 0; i < sizeGraph; i++)
        {
            for(int n = 0; n < oldGraph[i].countArcs; n++)
            {
                int srcFound = 0;
                int dstFound = 0;

                if(!__findNodes(oldGraph[i].name, oldGraph[i].arcs[n]->name, &srcFound, &dstFound))
                    return false;

                if(srcFound < 0 || dstFound < 0)
                    return false;

                if(!__connectNodes(srcFound, dstFound))
                    return false;
            }

            delete[] oldGraph[i].arcs;
        }

        return true;
    }

//=============================================================================
//  METODI ARCHI PRIVATI CLASSE
//=============================================================================

    bool __resizeArcs(int nodeEntry, int newSize)
    {
        if(__nodes[nodeEntry].countArcs < 0)
            return false;

        if(newSize <= 0)
        {
            delete[] __nodes[nodeEntry].arcs;

            __nodes[nodeEntry].arcs      = NULL;
            __nodes[nodeEntry].countArcs = 0;

            return true;
        }

        Node** tmpPtr                   = __nodes[nodeEntry].arcs;
        int tmpCount                    = __nodes[nodeEntry].countArcs;
        int current                     = 0;
        __nodes[nodeEntry].countArcs    = newSize;
        __nodes[nodeEntry].arcs         = new Node*[__nodes[nodeEntry].countArcs];

        if(!__nodes[nodeEntry].arcs)
        {
            __nodes[nodeEntry].countArcs = tmpCount;
            __nodes[nodeEntry].arcs      = tmpPtr;

            return false;
        }

        if (tmpCount > newSize) current = newSize;
        else current = tmpCount;

        for(int i = 0; i < current; i++)
            __nodes[nodeEntry].arcs[i] = tmpPtr[i];

        if (tmpCount < newSize)
            for(int i = current; i < newSize; i++)
                __nodes[nodeEntry].arcs[i] = NULL;

        delete[] tmpPtr;
        tmpPtr = NULL;

        return true;
    }

    bool __connectNodes(int srcId, int dstId, bool oriented = false)
    {
        bool found = false;
        int index  = __nodes[srcId].countArcs;

        for(int i = 0; i < index; i++)
            if(__nodes[srcId].arcs[i] == &__nodes[dstId])
            {
                found = true;
                break;
            }

        if(!found)
        {
            if(!__resizeArcs(srcId, index + 1))
                return false;

            __nodes[srcId].arcs[index] = &__nodes[dstId];
        }

        if(oriented || (srcId == dstId)) return true;
        else return (true && __connectNodes(dstId, srcId, true));
    }

    bool __disconnectNodes(int srcId, int dstId, bool oriented = false)
    {
        int deleted  = false;
        int countSrc = __nodes[srcId].countArcs;

        if(countSrc <= 0)
            return false;

        for(int i = 0; i < countSrc; i++)
            if(__nodes[srcId].arcs[i]->nodeId == dstId)
            {
                for(int n = i; n < countSrc; n++)
                {
                    if((n + 1) >= countSrc)
                        continue;

                    __nodes[srcId].arcs[n] = __nodes[srcId].arcs[n + 1];
                }

                deleted = true;
                break;
            }

        if(deleted)
        {
            if(!__resizeArcs(srcId, __nodes[srcId].countArcs - 1))
                return false;
        }

        if(oriented || (srcId == dstId)) return true;
        else return (true && __disconnectNodes(dstId, srcId, true));
    }

public:
//=============================================================================
//  COSTRUTTORE - DISTRUTTORE
//=============================================================================

    Graph()
    {
        __countNodes    = 0;
        __nodes         = NULL;
    }

    ~Graph()
    {
        if(!__nodes)
            return;

        delete[] __nodes;

        __nodes         = NULL;
        __countNodes    = 0;
    }

//=============================================================================
//  METODI NODI PUBBLICI CLASSE
//=============================================================================

    int getLength()
    {
        return __countNodes;
    }

    Node* appendNode()
    {
        if(!__resize(__countNodes + 1))
            return NULL;

        return &__nodes[__countNodes - 1];
    }

    Node* insertNode(int position)
    {
        if(position > __countNodes || position < 0)
            return NULL;

        if(!__resize(__countNodes + 1, position))
            return NULL;

        return &__nodes[position];
    }

    bool removeLast()
    {
        if(!__connectionCleaner(__countNodes - 1))
            return false;

        return __resize(__countNodes - 1);
    }

    bool removeAt(int position)
    {
        if(position >= __countNodes || position < 0)
            return false;

        if(!__connectionCleaner(position))
            return false;

        return __resize(__countNodes - 1, position, true);
    }

    bool resizeGraph(int size, bool force = false)
    {
        if(size < 0 || (size < __countNodes && !force))
            return false;

        if(size < __countNodes)
        {
            for(int i = size; i < __countNodes; i++)
                if(!__connectionCleaner(i))
                    return false;
        }

        return __resize(size);
    }

    Node* getNodeAt(int position)
    {
        if(position >= __countNodes || position < 0)
            return NULL;

        return &__nodes[position];
    }

    void outputGraph()
    {
        std::cout << "==========================================================" << std::endl;
        std::cout << " Total Nodes:  " << __countNodes;

        if(__nodes != NULL && __countNodes > 0)
        {
            std::cout << std::endl << std::endl;

            for(int i = 0; i < __countNodes; i++)
            {
                Node tmp = __nodes[i];
                std::cout << "   +-->\t Node-ID< " << tmp.nodeId << " >:  NAME = '" << tmp.name << "'" << std::endl;

                if(tmp.countArcs > 0)
                {
                    for(int n = 0; n < tmp.countArcs; n++)
                    {
                        Node* tmpArc = tmp.arcs[n];
                        std::cout << "       \t  Arc-TO< " << tmpArc->nodeId << " >:  NAME = '" << tmpArc->name << "'" << std::endl;
                    }

                    if(tmp.countArcs > 0)
                        std::cout << std::endl;
                }
            }
        }

        std::cout << std::endl << "==========================================================" << std::endl;
    }

//=============================================================================
//  METODI ARCHI PUBBLICI CLASSE
//=============================================================================

    bool appendArc(std::string src, std::string dst, bool oriented = false)
    {
        int srcFound = 0;
        int dstFound = 0;

        if(!__findNodes(src, dst, &srcFound, &dstFound))
            return false;

        if(srcFound < 0 || dstFound < 0)
            return false;

        return __connectNodes(srcFound, dstFound, oriented);
    }

    bool appendArc(int src, int dst, bool oriented = false)
    {
        if(__countNodes <= 0)
            return false;

        if(src < 0 || src > __countNodes)
            return false;

        if(dst < 0 || dst > __countNodes)
            return false;

        return __connectNodes(src, dst, oriented);
    }

    bool removeArc(std::string src, std::string dst, bool oriented = false)
    {
        int srcFound = 0;
        int dstFound = 0;

        if(!__findNodes(src, dst, &srcFound, &dstFound))
            return false;

        if(srcFound < 0 || dstFound < 0)
            return false;

        return __disconnectNodes(srcFound, dstFound, oriented);
    }

    bool removeArc(int src, int dst, bool oriented = false)
    {
        if(__countNodes <= 0)
            return false;

        if(src < 0 || src > __countNodes)
            return false;

        if(dst < 0 || dst > __countNodes)
            return false;

        return __disconnectNodes(src, dst, oriented);
    }

};

#endif

//=============================================================================
//                                   EOF
//=============================================================================
